function log(x){
  console.log(x);
}

function logDecorator(func) {
  return function (x) {
    console.log("Start");
    const res =  func(x);
    console.log("Finish");
  }
}

const decorated = logDecorator(log);

log('Mika');
decorated('Pera');